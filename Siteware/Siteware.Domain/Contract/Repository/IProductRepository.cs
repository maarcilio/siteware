﻿using Siteware.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Domain.Contract.Repository
{
    public interface IProductRepository : IRepositoryBase<Product>, IDisposable
    {
    }
}
