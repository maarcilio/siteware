﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Siteware.Domain.Contract.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Create(TEntity model);

        void Update(TEntity model);

        void Delete(TEntity model);

        TEntity GetById(int Id);

        ICollection<TEntity> GetAll();

        ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        void Dispose();
    }
}
