﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Siteware.Domain.Contract.Service
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        void Create(TEntity model);

        void Update(TEntity model);

        void Delete(int Id);

        TEntity GetById(int Id);

        ICollection<TEntity> GetAll();

        ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        void Dispose();
    }
}
