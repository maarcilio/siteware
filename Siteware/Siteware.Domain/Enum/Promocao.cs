﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Domain.Enum
{
    public enum Promocao : int
    {
        [Display(Name = "Pague 1 leve 2")]
        Pague1Leve2 = 1,
        [Display(Name = "3 por 10 reais")]
        TresPor10reais = 2
    }
}
