﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Siteware.Domain.Enum;
using Siteware.Domain.Service;

namespace Siteware.Domain.Models
{
    public class ItemCartShopping
    {
        public Product Product { get; set; }

        public int Quantidade { get; set; }

        private decimal PrecoTotal { get; set; }

        public decimal CalculatePromotionalPrice()
        {
            PromocaoService service = new PromocaoService();
            switch (this.Product.Promocao)
            {
                case Promocao.Pague1Leve2:
                    this.PrecoTotal = service.Pague1Leve2(this);
                    break;
                case Promocao.TresPor10reais:
                    this.PrecoTotal = service.TresPor10reais(this);
                    break;
                default:
                    this.PrecoTotal = this.Product.Preco * this.Quantidade;
                    break;
            }
            return this.PrecoTotal;
        }
    }
}
