﻿using Siteware.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Domain.Models
{
    public class Product
    {
        public virtual int Id { get; set; }

        public virtual string Nome { get; set; }

        public virtual decimal Preco { get; set; }

        public virtual Promocao Promocao { get; set; }
    }
}
