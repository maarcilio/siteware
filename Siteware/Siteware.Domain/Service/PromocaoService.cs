﻿using Siteware.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Domain.Service
{
    public class PromocaoService
    {
        public decimal Pague1Leve2(ItemCartShopping model)
        {
            decimal PrecoTotal = 0;
            for (int i = 1; i <= model.Quantidade; i++)
            {
                if (i%2 != 0)
                {
                    PrecoTotal += model.Product.Preco;
                }
            }
            return PrecoTotal;
        }

        public decimal TresPor10reais(ItemCartShopping model)
        {
            decimal PrecoTotal = 0;
            for (int i = 1; i <= model.Quantidade; i++)
            {
                if (i % 3 == 0)
                {
                    PrecoTotal -= (model.Product.Preco*2);
                    PrecoTotal += 10;
                }
                else
                {
                    PrecoTotal += model.Product.Preco;
                }
            }
            return PrecoTotal;
        }
    }
}
