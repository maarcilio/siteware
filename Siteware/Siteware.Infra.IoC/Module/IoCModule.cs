﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Siteware.Domain.Contract.Repository;
using Siteware.Infra.Repository;
using Siteware.Domain.Contract.Service;
using Siteware.Service;

namespace Siteware.Infra.IoC.Module
{
    public class IoCModule : NinjectModule
    {
        public static void LoadModule(IKernel kernel)
        {
            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IProductRepository>().To<ProductRepository>();
            
            kernel.Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            kernel.Bind<IProductService>().To<ProductService>();
        }

        public override void Load()
        {
            
        }
    }
}
