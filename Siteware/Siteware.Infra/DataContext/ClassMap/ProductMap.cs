﻿using FluentNHibernate.Mapping;
using Siteware.Domain.Enum;
using Siteware.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Infra.DataContext.ClassMap
{
    public class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Table("Product");

            Id(x => x.Id);

            Map(x => x.Nome).Column("Nome");
            Map(x => x.Preco).Column("Preco");
            Map(x => x.Promocao).Column("Promocao").CustomType(typeof(Promocao));
        }
    }
}
