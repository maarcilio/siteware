﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Infra.DataContext
{
    public class FluentySessionFactory
    {
        private const string ConnectionString = @"data source=184.168.47.15;User Id=MarcilioAlves;Password=M@rcilio1993;database=Camarote";
        private static ISessionFactory session;

        public static ISessionFactory CreatSession()
        {
            if (session != null)
                return session;

            IPersistenceConfigurer configDb = MsSqlConfiguration.MsSql2012.ConnectionString(ConnectionString);

            var configMap = Fluently.Configure().Database(configDb).Mappings(x => x.FluentMappings.AddFromAssemblyOf<ClassMap.ProductMap>());

            session = configMap.BuildSessionFactory();

            return session;
        }

        public static ISession OpenSession()
        {
            return CreatSession().OpenSession();
        }

        public static void CloseSesssion()
        {
            session.Close();
        }
    }
}
