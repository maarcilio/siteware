﻿using Siteware.Domain.Contract.Repository;
using Siteware.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Infra.Repository
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
    }
}
