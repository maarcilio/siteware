﻿using Siteware.Domain.Contract.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Siteware.Infra.DataContext;
using NHibernate;
using NHibernate.Linq;
using System.Linq.Expressions;

namespace Siteware.Infra.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity>, IDisposable where TEntity : class
    {
        public void Create(TEntity model)
        {
            using (ISession session = FluentySessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(model);

                        transaction.Commit();
                    }
                    catch
                    {
                        if (transaction.WasCommitted)
                            transaction.Rollback();
                    }
                }
            }
        }

        public void Update(TEntity model)
        {
            using (ISession session = FluentySessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(model);

                        transaction.Commit();
                    }
                    catch
                    {
                        if (transaction.WasCommitted)
                            transaction.Rollback();
                    }
                }
            }
        }

        public void Delete(TEntity model)
        {
            using (ISession session = FluentySessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(model);

                        transaction.Commit();
                    }
                    catch
                    {
                        if (transaction.WasCommitted)
                            transaction.Rollback();
                    }
                }
            }
        }

        public TEntity GetById(int Id)
        {
            using (ISession session = FluentySessionFactory.OpenSession())
            {
                return session.Get<TEntity>(Id);
            }
        }

        public ICollection<TEntity> GetAll()
        {
            using (ISession session = FluentySessionFactory.OpenSession())
            {
                return session.Query<TEntity>().ToList();
            }
        }

        public ICollection<TEntity> GetAll(Expression<Func<TEntity,bool>> predicate)
        {
            using (ISession session = FluentySessionFactory.OpenSession())
            {
                return session.Query<TEntity>().Where(predicate).ToList();
            }
        }

        public void Dispose()
        {
            FluentySessionFactory.CloseSesssion();
        }
    }
}
