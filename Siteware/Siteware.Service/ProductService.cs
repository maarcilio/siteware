﻿using Siteware.Domain.Contract.Repository;
using Siteware.Domain.Contract.Service;
using Siteware.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Service
{
    public class ProductService : ServiceBase<Product>, IProductService
    {
        private readonly IProductRepository _repository;

        public ProductService(IProductRepository repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
