﻿using Siteware.Domain.Contract.Repository;
using Siteware.Domain.Contract.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Siteware.Service
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity>,IDisposable where TEntity : class
    {
        IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public void Create(TEntity model)
        {
            _repository.Create(model);
        }

        public void Update(TEntity model)
        {
            _repository.Update(model);
        }

        public void Delete(int Id)
        {
            var model = this.GetById(Id);
            _repository.Delete(model);
        }

        public TEntity GetById(int Id)
        {
            return _repository.GetById(Id);
        }

        public ICollection<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.GetAll(predicate);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
