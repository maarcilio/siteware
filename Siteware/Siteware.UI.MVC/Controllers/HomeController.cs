﻿using Newtonsoft.Json;
using Siteware.Domain.Contract.Service;
using Siteware.Domain.Models;
using Siteware.UI.MVC.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Siteware.UI.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _service;

        public HomeController(IProductService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CartShopping()
        {
            var cartCookie = Request.Cookies[Utility.NameCookieCart];
            List<ItemCartShopping> carrinho = new List<ItemCartShopping>();
            if (cartCookie == null)
            {
                return View(new List<ItemCartShopping>());
            }

            carrinho = FactoryCartShopping(cartCookie);

            return View(carrinho);
        }

        public PartialViewResult GetCartShoppingMenu()
        {
            var cartCookie = Request.Cookies[Utility.NameCookieCart];
            List<ItemCartShopping> carrinho = new List<ItemCartShopping>();
            if (cartCookie == null)
            {
                return PartialView("_CartShoppingMenuPartial", carrinho);
            }
            carrinho = FactoryCartShopping(cartCookie);

            return PartialView("_CartShoppingMenuPartial", carrinho);
        }

        public ActionResult Checkout()
        {
            if (Request.Cookies[Utility.NameCookieCart] != null)
            {
                HttpCookie myCookie = new HttpCookie(Utility.NameCookieCart);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            return RedirectToAction("Index");
        }

        #region [ Helpers ]

        private List<ItemCartShopping> FactoryCartShopping(HttpCookie cartCookie)
        {
            List<ItemCartShopping> carrinho = JsonConvert.DeserializeObject<List<ItemCartShopping>>(cartCookie.Value);

            List<int> listaId = carrinho.Select(x => x.Product.Id).ToList();

            var products = _service.GetAll(x => listaId.Contains(x.Id)).ToList();

            for (int i = 0; i < carrinho.Count; i++)
            {
                carrinho[i].Product = products.FirstOrDefault(x => x.Id == carrinho[i].Product.Id);
            }

            //Remover do carrinho o produto não encontrado
            if (carrinho.Any(x => x.Product == null))
            {
                carrinho.RemoveAll(x => x.Product == null);

                var json = JsonConvert.SerializeObject(carrinho);
                var cookie = new HttpCookie(Utility.NameCookieCart, json);
                cookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Remove(Utility.NameCookieCart);
                Response.SetCookie(cookie);
            }

            return carrinho;
        }

        #endregion
    }
}