﻿using Newtonsoft.Json;
using Siteware.Domain.Contract.Service;
using Siteware.Domain.Models;
using Siteware.UI.MVC.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Siteware.UI.MVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        #region [ CRUD ]

        public ActionResult Index()
        {
            return View(_service.GetAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Product model)
        {
            if (ModelState.IsValid)
            {
                _service.Create(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int Id)
        {
            return View(_service.GetById(Id));
        }

        [HttpPost]
        public ActionResult Edit(Product model)
        {
            if (ModelState.IsValid)
            {
                _service.Update(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Details(int Id)
        {
            return View(_service.GetById(Id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int Id)
        {
            try
            {
                _service.Delete(Id);
                return Json(new { result = true, message = "Excluido com sucesso" }, JsonRequestBehavior.AllowGet);
            }
            catch { }

            return Json(new { result = false, message = "Ocorreu um erro" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public PartialViewResult ProductListing()
        {
            return PartialView("_ProductListing", _service.GetAll());
        }

        [HttpPost]
        public JsonResult AddProductCart(ItemCartShopping model)
        {
            var cartCookie = Request.Cookies[Utility.NameCookieCart];
            List<ItemCartShopping> carrinho = new List<ItemCartShopping>();
            if (cartCookie != null && !string.IsNullOrEmpty(cartCookie.Value))
            {
                carrinho = JsonConvert.DeserializeObject<List<ItemCartShopping>>(cartCookie.Value);
                //Get item pelo Id para somar caso exista
                var item = carrinho.FirstOrDefault(x => x.Product.Id == model.Product.Id);
                if (item == null)
                {
                    carrinho.Add(model);
                }
                else
                {
                    carrinho.Remove(item);
                    item.Quantidade += model.Quantidade;
                    carrinho.Add(item);
                }
            }
            else
                carrinho.Add(model);

            var json = JsonConvert.SerializeObject(carrinho);
            var cookie = new HttpCookie(Utility.NameCookieCart, json);
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Remove(Utility.NameCookieCart);
            Response.SetCookie(cookie);

            return Json(new { data = carrinho, result = true, message = "Adicionado ao carrinho." });
        }

        [HttpPost]
        public JsonResult RemoveProductCart(int Id)
        {
            var cartCookie = Request.Cookies[Utility.NameCookieCart];
            List<ItemCartShopping> carrinho = new List<ItemCartShopping>();
            if (cartCookie != null && !string.IsNullOrEmpty(cartCookie.Value))
            {
                carrinho = JsonConvert.DeserializeObject<List<ItemCartShopping>>(cartCookie.Value);
                //Get item pelo Id para somar caso exista
                carrinho.RemoveAll(x => x.Product.Id == Id);
            }

            var json = JsonConvert.SerializeObject(carrinho);
            var cookie = new HttpCookie(Utility.NameCookieCart, json);
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.SetCookie(cookie);
            return Json(new { data = carrinho, result = true, message = "Produto Removido do carrinho." });
        }
    }
}