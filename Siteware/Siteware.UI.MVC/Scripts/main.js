﻿$(function () {

    $("[data-addProductCart]").on("click", function (e) {
        e.preventDefault();
        var id = $(this).data("idproduct");
        var item = {
            Product:
                {
                    Id: $(this).data("idproduct"),
                    Nome: $(this).data("nome"),
                    Preco: $(this).data("preco"),
                    Promocao: $(this).data("promocao")
                },
            Quantidade: parseInt($(this).parent().parent().find("input").val())
        };
        $.ajax({
            url: "/Product/AddProductCart",
            data: item,
            method: "POST",
            success: function (data) {
                if (data.result == true) {
                    $.get("/Home/GetCartShoppingMenu", function (response) {
                        $("#CartShopping").html(response);
                    });
                }
            }
        });
    });
});