﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Siteware.UI.MVC.Util.Helpers
{
    public class EnumExtension
    {

        public static string GetDisplayName<T>(T value)
        {
            try
            {
                if (value == null || Convert.ToInt32(value) == 0)
                {
                    return value.ToString();
                }
                var fieldInfo = value.GetType().GetField(value.ToString());

                var descriptionAttributes = fieldInfo.GetCustomAttributes(
                    typeof(DisplayAttribute), false) as DisplayAttribute[];

                if (descriptionAttributes == null) return string.Empty;
                return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
            }
            catch
            {
                return value.ToString();
            }
        }

        public static string GetDisplayName<T>(T value,string defulValue)
        {
            try
            {
                if (value == null || Convert.ToInt32(value) == 0)
                {
                    return defulValue;
                }
                var fieldInfo = value.GetType().GetField(value.ToString());

                var descriptionAttributes = fieldInfo.GetCustomAttributes(
                    typeof(DisplayAttribute), false) as DisplayAttribute[];

                if (descriptionAttributes == null) return string.Empty;
                return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
            }
            catch
            {
                return value.ToString();
            }
        }
    }
}